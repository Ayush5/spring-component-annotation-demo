package com.esewa.springdemo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Pancard {

    @Value("25")
    public String panNumber;
    @Value("#{employee.name}")
    public String pancardHolderName;

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getPancardHolderName() {
        return pancardHolderName;
    }

    public void setPancardHolderName(String pancardHolderName) {
        this.pancardHolderName = pancardHolderName;
    }

    @Override
    public String toString() {
        return "Pancard{" +
                "panNumber='" + panNumber + '\'' +
                ", pancardHolderName='" + pancardHolderName + '\'' +
                '}';
    }
}
